/*
*此程式是身分證產生器
*Created in 20201027 BY Shengwen
*下列簡述身分證規則
*第一個為英文字母 A->10	,B->11	,C->12	,D->13	,E->14	,F->15	,G->16	,H->17	,J->18	,K->19	,M->21	,
				　N->22	,P->23	,Q->24	,T->27	,U->28	,V->29	,Z->33	,W->32	,X->30	,I- >34	,O->35	
*第二位為性別1 2 有兩種可能
*第三位到第九位為流水碼
*第十位為檢查碼 其計算方法是從右邊開始依序乘1,2,......,8+英文字對應到之數字之個位數乘上乘9+十位數全部和除以10再取餘數
 如果餘數是0 檢查碼則是0 若否 則以10扣餘數算出
 因此程式是身分證產生器，不涉及計算機率問題，雖然以下方法分給上表之字母及不同的數字的機率並不相等，但由於其差異很小，此程式忽略
*/
#include <stdlib.h> /*srand函數 rand函數用 */
#include <stdio.h>
int main(void)
{
		srand(abs( time(NULL) )); /*以時間作種子*/
		char c;    /*存第一位英文字*/
		int num = 1; /*存入身分證字號後九碼*/
		int checknumber = 0;  /*計算檢查碼用*/
		int i; /*迴圈專用*/
		int tem; /* 暫存用 */
		/*以亂數表產出第一個英文字母,if遇到沒有在第五六行的字母則重新取*/  
		do
			c = 65 + (rand() % 27);
		while (c == 'L' || c == 'R' || c == 'S' || c == 'Y');
		
		/*算出第一個數字和checknumber*/
		num += rand() % 2;
		checknumber += num * 8;
		
		/*選第二位到第八位之數字，並將他存至num變數中*/
		for (i = 0; i < 7; i++)
		{
			num *= 10;
			tem = rand() % 10;
			num += tem;
			checknumber += tem * (7 - i);
		}
		
		
		/*依據規則算英文字母的checknumber*/
		switch (c)
		{
		case 'A':
			checknumber += 1;
		case 'B':
			checknumber += 10;
			break;
		case 'C':
			checknumber += 19;
			break;
		case 'D':
			checknumber += 28;
			break;
		case 'E':
			checknumber += 37;
			break;
		case 'F':
			checknumber += 46;
			break;
		case 'G':
			checknumber += 55;
			break;
		case 'H':
			checknumber += 64;
			break;
		case 'I':
			checknumber += 39;
			break;
		case 'J':
			checknumber += 73;
			break;
		case 'K':
			checknumber += 82;
			break;
		case 'M':
			checknumber += 11;
			break;
		case 'N':
			checknumber += 20;
			break;
		case 'O':
			checknumber += 48;
			break;
		case 'P':
			checknumber += 29;
			break;
		case 'Q':
			checknumber += 38;
			break;
		case 'T':
			checknumber += 65;
			break;
		case 'U':
			checknumber += 74;
			break;
		case 'V':
			checknumber += 83;
			break;
		case 'W':
			checknumber += 21;
			break;
		case 'X':
			checknumber += 3;
			break;
		case 'Z':
			checknumber += 30;
			break;

		};

		/*求出最後一位數字*/
		num *= 10;
		if (checknumber % 10)  num += 10 - (checknumber % 10);
		printf("身分證產生器產生出%c%d", c, num);
	return 0;
}